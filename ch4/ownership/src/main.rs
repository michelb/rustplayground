fn main() {
    { // some block scope example
        let s = 4;
    }
    {
        let mut s = String::from("blablabla");
        s.push_str(" and more blablablabla");
        takes_ownership(s);
        // println!("this shouldn't compile {}", s);
        // it doesn't
        let s = String::from(" another string   ");
        takes_ownership(s.clone());
        println!("this should compile {}", s);
        let (s, len) = calculate_length(s);
        println!("'{}' has {} chars", s, len);
        let len = calc_length_2(&s);
        println!("'{}' still has {} chars but we didn't invalidate it", s, len);
        let mut s2 = s.clone();
        //let len = calc_length_and_trim(&mut s2);
        //println!("'{}' has {} chars without whitespace", s, len);
    }

    //let s = dangle();
    {
        let test = String::from("abcd e f ghijkl");
        let first_word = first_word(&test);
        println!("the first word in test is '{}'", first_word);
    }
    {
        const ABRA : &str = "abrac adabra";
        println!("the first word in '{}' is '{}'", ABRA, first_word(ABRA));
    }
}

fn first_word(s : &str) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[..i];
        }
    }
    &s[..]
}

fn takes_ownership(s : String) {
    println!("{} is now mine", s);
}

fn calculate_length(s: String) -> (String, usize) {
    let len : usize = s.len();
    (s, len)
}

fn calc_length_2(s: &String) -> usize {
    s.len()
}

/* doesn't work. don't know enough Rust to figure out why or how to fix it
fn calc_length_and_trim(s: &mut String) -> usize {
    s = s.trim().to_owned();
    s.len()
}
*/
/*
fn dangle() -> &String {
    let s = String::from("dingle");
    &s
}
*/
