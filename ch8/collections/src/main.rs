
fn main() {
    let vec : Vec<i32> = Vec::new();
    let vec2 = vec![1, 2, 3, 4, 5];
    let mut vec3 = vec!["h", "e", "l", "l", "o"];
    vec3.push(" ");
    let mut vec4 = vec!["w", "o", "r", "l", "d"];
    vec3.append(&mut vec4);
    let h = &vec3[0];
    for i in &vec3 {
        println!("next char: {}", i);
    }
    /*
    for i in &mut vec3 {
        if i.to_string() == " ".to_string() {
            i = "_";
        }
    }*/

    enum VariantField {
        Int(i32),
        Float(f32),
        Text(String),
    };

    let vec4 = vec![VariantField::Int(42), VariantField::Float(3.14159), VariantField::Text(String::from("lsdkfjsdf"))];

    use std::collections::HashMap;
    let mut kvs = HashMap::new();
    kvs.insert(String::from("bla"), 12);
    kvs.insert(String::from("blabla"), 41);
    for (k,v) in &kvs {
        println!("{} => {}", k, v);
    }
}
