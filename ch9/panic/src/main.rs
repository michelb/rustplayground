fn always_fail() -> Result<String,i32> {
    return Err(5);
}

fn never_fail() -> Result<String,f32> {
    return Ok(String::from("blabbity bla-bla"));
}

fn main() {
    println!("Hello, world!");
    //panic!();
    match always_fail() {
        Ok(s) => {
            println!("This shouldn't happen... {}", s);
            panic!();
        },
        Err(num) => {
            println!("Failed as expected with code {}", num);
        }
    }
    println!("result of calling never_fail(): {}", never_fail().unwrap());
}
