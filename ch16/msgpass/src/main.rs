use std::{sync::mpsc, thread, time::Duration};

fn main() {
    let (source, sink) = mpsc::channel();
    let source_2 = mpsc::Sender::clone(&source);
    thread::spawn(move || {
        let vals = vec![
            String::from("hi"),
            String::from("from"),
            String::from("the"),
            String::from("thread"),
        ];

        for val in vals {
            source_2.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("more"),
            String::from("messages"),
            String::from("for"),
            String::from("you"),
        ];

        for val in vals {
            source.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    for received in sink {
        println!("Got: {}", received);
    }

    // no join necessary
    // apparently sink knows not to stop until source has been dropped. good to know
}
