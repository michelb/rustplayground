pub trait Draw {
    fn draw(&self);
}

pub struct Screen {
    pub components : Vec<Box<dyn Draw>>,
}

impl Screen {
    pub fn run(&self) {
        for c in self.components.iter() {
            c.draw();
        }
    }
}

pub struct Button {
    pub width: u32,
    pub height: u32,
    pub label: String,
}

impl Draw for Button {
    fn draw(&self) {
        // code to draw a button
    }
}

#[cfg(test)]
mod tests {

    use std::rc::Rc;
    use std::cell::RefCell;

    struct MockButton {
        was_drawn: Rc<RefCell<bool>>, 
    }

    impl Draw for MockButton {
        fn draw(&self) {
            *self.was_drawn.borrow_mut() = true;
        }
    }

    use crate::*;
    #[test]
    fn run_calls_draw() {
        let flag = Rc::new(RefCell::new(false));
        let button = Box::new(MockButton{was_drawn: Rc::clone(&flag)});
        let screen = Screen{
            components: vec![button]
        };
        assert!(!*(*flag).borrow());
        screen.run();
        assert!(*(*flag).borrow());
    }
}
