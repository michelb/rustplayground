//use std::{collections::HashMap, fmt};
use std::io::{self, Write};
use std::collections::*;

/*
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
*/

// this seems to be here to intentionally show that it doesn't conflict
// with the one in front_of_house::server
fn serve_order() {

}

mod back_of_house {
    fn fix_incorrect_order() {
        cook_order();
        super::serve_order();
    }

    fn cook_order() {}

    pub struct Breakfast {
        pub toast: String,
        seasonal_fruit: String,
    }

    impl Breakfast {
        pub fn summer(toast: &str) -> Breakfast {
            Breakfast{
                toast: String::from(toast),
                seasonal_fruit: String::from("peaches"),
            }
        }
    }

    pub enum Appetizer {
        Soup,
        Salad,
    }
}

mod front_of_house;
pub use crate::front_of_house::hosting;
use crate::back_of_house::Appetizer as Apps;

pub fn eat_at_restaurant() {
    //crate::front_of_house::hosting::add_to_waitlist();
    // or
    //front_of_house::hosting::add_to_waitlist();
    let mut meal = back_of_house::Breakfast::summer("rye");
    meal.toast = String::from("wheat");
    println!("I'd like {} toast, please", meal.toast);

    let order1 = Apps::Salad;
    let order2 = Apps::Soup;
}