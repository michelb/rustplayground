#[derive(Debug)]
enum State {
	Alabama, Connecticut, Texas
}

#[derive(Debug)]
enum Coin {
	Penny,
	Nickel,
	Dime,
	Quarter(State),
}

fn value_in_cents(coin: &Coin) -> u8 {
	match coin {
		Coin::Penny => 1,
		Coin::Nickel => 5,
		Coin::Dime => 10,
		Coin::Quarter(state) => {
			println!("Woohoo a state quarter from {:?}", state);
			25
		}
	}
}

fn main() {
	let change = [Coin::Penny, Coin::Nickel, Coin::Nickel, Coin::Quarter(State::Texas)];
	let mut total = 0;
	for coin in change.iter() {
		total += value_in_cents(coin);
	}
	println!("total value is {} cents (i still don't know how to cast ints to floats)", total);
}
