#[derive(Debug)]
enum IpAddr {
    V4(u8, u8, u8, u8),
    V6(String),
}

fn maybe_five(x: i32) -> Option<i32> {
    if x > 7 { 
        Some(5)
    } else {
        None
    }
}

fn main() {
    let home = IpAddr::V4(127, 0, 0, 1);
    let loopback = IpAddr::V6(String::from("::1"));
    println!("home = {:?}", home);
    println!("loopback = {:?}", loopback);
    let five = maybe_five(12).expect("Expected 5");
}
