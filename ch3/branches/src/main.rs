fn main() {
    let number = 3;
    if number > 5 {
        println!("condition was true");
    } else {
        println!("condition was false");
    }
    let oddly_enough = if number % 2 == 0 {
        "even"
    } else {
        "odd"
    };
    println!("number is {}", oddly_enough);

    let mut loops = 0;
    loops = loop {
        loops += 1;
        if loops == 20 {
            break loops;
        }
    };
    println!("loop ran {} times", loops);

    while loops > 5 {
        println!("decrementing. current val {}", loops);
        loops -= 1;
    }

    let a = [1, 2, 3, 4, 5, 6, 7];
    for element in a.iter() {
        println!("next element in a is {}", element);
    }
    for element in (1..10) {
        println!("element is {}", if element % 2 == 0 { "even" } else { "odd" });
    }
}
