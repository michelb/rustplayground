fn main() {
    let mut x = 5;
    println!("The value of x is {}", x);
    x = 6;
    println!("The value of x is now {}", x);
    let x = x + 1; // shadow
    println!("The value of x is now {}", x);
    let x = x * 2;
    println!("The value of x is now {}", x);

    let y : f32 = 5.0;
    let z : f64 = (y / 0.002).into();

    let t = true;
    let f = !t;

    const c : char = '💩';

    let a = ("a man", "a plan", 2.1234, 'p', "anama");
    let (_, plan, _, _, _) = a;
    println!("I love it when {} comes together", plan);
    println!("The password is {}", a.2);

    let b = ["a", "b", "cd", "xyz"];

    let i = 5;
    //println!("This should panic {}", b[i]);
    //const idx : usize = 100;
    //println!("But does this even build? {}", b[idx]);
    // no. no it does not. excellent
}
