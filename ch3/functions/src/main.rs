fn main() {

    let x = {
        let y = 2.0;
        let z = 3.14159;
        (y * y) * z
    };

    another_function(x);
    println!("2 + 3 = {}", five());
    println!("10! = {}", factorial(10));
}

fn five() -> i32 {
    5
}

fn factorial(x: i32) -> i32 {
    if x <= 1 {
        1
    } else {
        x * factorial(x - 1)
    }
}

fn another_function(x: f32) {
    println!("x = {}", x);
}
