use std::error::Error;
use std::fs;
use std::env;

pub struct Config {
    pub query: String,
    pub filename: String,
    pub case_sensitive: bool
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 3 {
            Err("Not enough arguments given")
        } else {
            let case_sensitive = env::var("CASE_INSENSITIVE").is_err();
            let query = args[1].clone();
            let filename = args[2].clone();
            Ok(Config { query, filename, case_sensitive })
        }
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let file_contents = fs::read_to_string(config.filename)?;
    let results = if config.case_sensitive {
        search(&config.query, &file_contents)
    } else {
        search_case_insensitive(&config.query, &file_contents)
    };
    for line in results {
        println!("{}", line);
    }
    Ok(())
}

pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();
    for line in contents.lines() {
        if line.contains(query) {
            results.push(line);
        }
    }
    results
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut results = Vec::new();
    let query = query.to_lowercase();
    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }
    results
}

#[cfg(test)]
mod tests {

    #[test]
    fn config_no_args() {
        let args: Vec<String> = vec![];
        assert!(super::Config::new(&args).is_err());
    }

    #[test]
    fn config_one_arg() {
        let args: Vec<String> = vec![String::from("appname")];
        assert!(super::Config::new(&args).is_err());
    }

    #[test]
    fn config_two_args() {
        let args: Vec<String> = vec![String::from("appname"), String::from("bla")];
        assert!(super::Config::new(&args).is_err());
    }

    #[test]
    fn config_three_args() {
        let args: Vec<String> = vec![
            String::from("appname"),
            String::from("bla"),
            String::from("borp"),
        ];
        assert!(!super::Config::new(&args).is_err());
    }

    #[test]
    fn config_bad_path() {
        // this test isn't very granular. yes, a bad path will cause a failure, but since
        // we're calling run() we can't know what actually caused the failure
        let args: Vec<String> = vec![
            String::from("appname"),
            String::from("bla"),
            String::from("borp"),
        ];
        if let Ok(config) = super::Config::new(&args) {
            assert!(super::run(config).is_err());
        } else {
            assert!(false);
        }
    }

    #[test]
    fn config_good_path() {
        // this test isn't very granular. yes, a bad path will cause a failure, but since
        // we're calling run() we can't know what actually caused the failure
        let args: Vec<String> = vec![
            String::from("appname"),
            String::from("bla"),
            String::from("poem.txt"),
        ];
        if let Ok(config) = super::Config::new(&args) {
            assert!(!super::run(config).is_err());
        } else {
            assert!(false);
        }
    }

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.";

        assert_eq!(vec!["safe, fast, productive."], super::search(query, contents));
    }

    #[test]
    fn case_insensitive() {
                let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            super::search_case_insensitive(query, contents)
        );
    }
}
