use std::rc::Rc;
use std::cell::RefCell;
use std::rc::Weak;

#[derive(Debug)]
enum List {
    Cons(i32, RefCell<Rc<List>>),
    Nil
}

impl List {
    fn tail(&self) -> Option<&RefCell<Rc<List>>> {
        use crate::List::{Cons, Nil};
        match self {
            Cons(_, item) => Some(item),
            Nil => None,
        }
    }
}

#[derive(Debug)]
struct Node {
    value: i32,
    children: RefCell<Vec<Rc<Node>>>,
    parent: RefCell<Weak<Node>>,
}

impl Node {
    fn new(value: i32) -> Rc<Node> {
        Rc::new(Node {
            value,
            children: RefCell::new(vec![]),
            parent: RefCell::new(Weak::new()),
        })
    }
    fn add_child(&self, p: &Rc<Node>) {
        self.children.borrow_mut().push(Rc::clone(&p));
    }
    fn set_parent(&self, p: &Rc<Node>) {
        *self.parent.borrow_mut() = Rc::downgrade(&p);
    }
    fn add_child_to_parent(child: &Rc<Node>, parent: &Rc<Node>) {
        parent.add_child(child);
        child.set_parent(parent);
    }
}

use std::ops::{Deref/*, Drop*/};

struct MyBox<T>(T);

impl<T> MyBox<T> {
    fn new(t: T) -> MyBox<T> {
        MyBox(t)
    }
}

impl<T> Deref for MyBox<T> {
    type Target = T;
    fn deref(&self) -> &T {
        &self.0
    }
}


/* Drop forbids specialization :/
impl<T> Drop for MyBox<str> {
    fn drop(&mut self) {
        println!("Destroying boxed string {}", &self.0);
    }
}*/


fn test_deref_coercion(name: &str) {
    println!("Hi, {}", name);
}

fn main() {
    use crate::List::{Cons, Nil};
    let a = Rc::new(Cons(5, RefCell::new(Rc::new(Nil))));

    println!("a initial rc count = {}", Rc::strong_count(&a));
    println!("a next item = {:?}", a.tail());

    let b = Rc::new(Cons(10, RefCell::new(Rc::clone(&a))));

    println!("a rc count after b creation = {}", Rc::strong_count(&a));
    println!("b initial rc count = {}", Rc::strong_count(&b));
    println!("b next item = {:?}", b.tail());

    if let Some(link) = a.tail() {
        *link.borrow_mut() = Rc::clone(&b);
    }

    println!("b rc count after changing a = {}", Rc::strong_count(&b));
    println!("a rc count after changing a = {}", Rc::strong_count(&a));

    // Uncomment the next line to see that we have a cycle;
    // it will overflow the stack
    //println!("a next item = {:?}", a.tail());

    let leaf = Node::new(3);

    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());

    let branch = Node::new(5);

    Node::add_child_to_parent(&leaf, &branch);

    println!("leaf parent = {:?}", leaf.parent.borrow().upgrade());

    println!("dumping tree: {:?}", branch);
}
