struct User {
    name: String,
    email: String,
    active: bool,
    sign_in_count: u64
}

fn build_user(name: &str, email: &str) -> User {
    User{
        name: String::from(name),
        email: String::from(email),
        active: true,
        sign_in_count: 1
    }
}

struct RGBColor(u8, u8, u8);
const RED : RGBColor = RGBColor(255, 0, 0);
const BLUE : RGBColor = RGBColor(0, 255, 0);
const GREEN : RGBColor = RGBColor(0, 0, 255);

fn main() {
    let mut user1 = User{
        name: String::from("Timmy McTimmons"),
        email: String::from("t.mc@timmons.net"),
        active: false,
        sign_in_count: 1
    };
    user1.active = true;
    let user2 = build_user("Timmy McTimmons","t.mc@timmons.net");
    let user3 = User{
        sign_in_count: 5,
        ..user2
    };
    
}


