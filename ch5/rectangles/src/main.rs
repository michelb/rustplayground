#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
    fn square(size: u32) -> Rectangle {
        Rectangle{width: size, height: size}
    }
}

fn main() {
    let rect = Rectangle{width: 50, height: 30};
    println!("The area of the rectangle {:?} is {}", rect, rect.area());
    let rect2 = Rectangle{width: 70, height: 100};
    println!("rect1 {:?} can hold rect2 {:?}? {}", rect, rect2, rect.can_hold(&rect2));
    println!("rect2 {:?} can hold rect1 {:?}? {}", rect2, rect, rect2.can_hold(&rect));
    let square = Rectangle::square(5);
}

