use std::{
    io::prelude::*,
    net::{
        TcpStream,
        TcpListener
    },
    fs,
    time::Duration,
    thread
};

use hello::ThreadPool;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    let pool = ThreadPool::new(4);
    for stream in listener.incoming() {
        let stream = stream.unwrap();
        pool.execute(|| {
            handle_connection(stream);
        });
    }
}

fn get_response_for_request(buffer : &[u8; 512]) -> String {
    let get_root = b"GET / HTTP/1.1\r\n";
    let get_sleep = b"GET /sleep HTTP/1.1\r\n";
    if buffer.starts_with(get_root) {
        let content = fs::read_to_string("hello.html").unwrap();
        return format!("HTTP/1.1 200 OK\r\n\r\n{}", content);
    } else if buffer.starts_with(get_sleep) {
        thread::sleep(Duration::from_secs(5));
        let content = fs::read_to_string("hello.html").unwrap();
        return format!("HTTP/1.1 200 OK\r\n\r\n{}", content);
    }
    let content = fs::read_to_string("404.html").unwrap();
    format!("HTTP/1.1 404 File not found\r\n\r\n{}", content)
}

fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 512];
    stream.read(&mut buffer).unwrap();
    stream.write(get_response_for_request(&buffer).as_bytes()).unwrap();
    stream.flush().unwrap();
}
